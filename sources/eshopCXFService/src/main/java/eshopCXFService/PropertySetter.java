package eshopCXFService;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(name = "PropertySetter", targetNamespace = "http://eshopCXFService/")
public interface PropertySetter {

    @WebMethod(operationName = "publishNewPrice", action = "urn:PublishNewPrice")
	public boolean publishNewPrice(String productName, double price);

    @WebMethod(operationName = "publishNewDescription", action = "urn:PublishNewDescription")
	public boolean publishNewDescription(String productName, String description);

    @WebMethod(operationName = "publishNewImageURL", action = "urn:PublishNewImageURL")
	public boolean publishNewImageURL(String productName, String imageURL);
}
