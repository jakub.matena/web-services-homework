package eshopCXFService;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.jws.WebService;

@WebService(targetNamespace = "http://eshopCXFService/", endpointInterface = "eshopCXFService.PropertySetter", portName = "PropertySetterImplPort", serviceName = "PropertySetterImplService")
public class PropertySetterImpl implements PropertySetter {

    private Set<String> products = Stream.of("Kindle 2020", "PocketBook 616")
            .collect(Collectors.toCollection(HashSet::new));

    @Override
    public boolean publishNewPrice(String productName, double price) {
        if (price < 1500)
            return false;
        return products.contains(productName);
    }

    @Override
    public boolean publishNewDescription(String productName, String description) {
        if (description.length() < 15)
            return false;
        return products.contains(productName);
    }

    @Override
    public boolean publishNewImageURL(String productName, String imageURL) {
        if (imageURL.length() < 10)
            return false;
        return products.contains(productName);
    }
}