package eshopRESTAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import jakarta.xml.bind.JAXBElement;

@Path("/products")
public class ProductsResource {
	

	@Context
	UriInfo uriInfo;
	
	private static Map<String, Product> products = new HashMap<String, Product>();
	private static boolean startup = true;
	    
	public ProductsResource() {
		if (startup) {
		    startup = false;
		    products.put("Kindle 2020", new Product("Kindle 2020", 2050, 15, "Basic Amazon Kindle device", "http://mydomain.cz/kindle-2020/image"));
		    products.put("PocketBook 616", new Product("PocketBook 616", 2130, 10, "Basic Pocketbook device", "http://mydomain.cz/pocketbook-616/image"));
		    products.put("USB cable", new Product("USB cable", 49, 5, "USB to microUSB cable", "http://mydomain.cz/usb-cable/image"));
		}
	}

	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public List<Product> getContacts() {
	    List<Product> returnedProducts = new ArrayList<Product>();
	    returnedProducts.addAll( products.values() );
	    return returnedProducts;
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("{name}")
	public Product getProduct(
	     	@PathParam("name") String name) {
		return products.get(name);
	}
	
	@GET
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("{name}/param/{param}")
	    public String getProduct(
	        	@PathParam("name") String name,
	        	@PathParam("param") String param) {
		if (products.containsKey(name)) {

			Product p = products.get(name);
			if (param.equals("name"))
				return p.getName();
			else if (param.equals("price"))
				return String.valueOf(p.getPrice());
			else if (param.equals("priceDifference"))
				return String.valueOf(p.getPriceDifference());
			else if (param.equals("description"))
				return p.getDescription();
			else if (param.equals("imageURL"))
				return p.getImageURL();
			else 
				return null;
		}
		else
			return null;
	}
	
	@POST
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("{name}")
	    public Response postProduct(
	        	@PathParam("name") String name,
	        	@DefaultValue("") @QueryParam("name") String newName,
	        	@DefaultValue("-1") @QueryParam("price") double newPrice,
	        	@DefaultValue("-1") @QueryParam("priceDifference") double newPriceDifference,
	        	@DefaultValue("") @QueryParam("description") String newDescription,
	        	@DefaultValue("") @QueryParam("imageURL") String newImageURL) {
		Response res;
		
		
		if (products.containsKey(name)) {
			
			res = Response.accepted().build();
			Product p = products.get(name);
			if (!newName.equals(""))
				p.setName(newName);
			if (newPrice != -1)
				p.setPrice(newPrice);
			if (newPriceDifference != -1)
				p.setPriceDifference(newPriceDifference);
			if (!newDescription.equals(""))
				p.setDescription(newDescription);
			if (!newImageURL.equals(""))
				p.setImageURL(newImageURL);
			if (newName.equals("") && newPrice == -1 && newPriceDifference == -1 
					&& newDescription.equals("") && newImageURL.equals("")) {
				res = Response.notModified().build();
			}
		}
		else {
			res = Response.status(404).build();
		}
		return res;
	}
	
	@DELETE
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("{name}")
	    public Response deleteProduct(
	        	@PathParam("name") String name) {
		Response res;
		
		
		if (products.containsKey(name)) {
			products.remove(name);
			res = Response.ok().build();
		}
		else {
			res = Response.notModified().build();
		}
		return res;
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Path("{name}")
	    public Response putContact(
	        	@PathParam("name") String name, JAXBElement<Product> product) {
		Product p = product.getValue();
		Response res;
		
		
		if (!name.equals(p.getName())) {
			res = Response.status(Response.Status.CONFLICT)
			         .entity("Names don't match").build();
			
		}
		else {
			if (products.containsKey(p.getName())) {
				products.put(p.getName(), p);
				res = Response.noContent().build();
			}
			else {
				products.put(p.getName(), p);
				res = Response.created(uriInfo.getAbsolutePath()).build();
			}
		}
		return res;
	}
}
