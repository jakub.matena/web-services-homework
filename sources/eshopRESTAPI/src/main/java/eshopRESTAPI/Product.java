package eshopRESTAPI;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Product {
  private String name ;
  private double price;
  private double priceDifference;
  private String description ;
  private String imageURL ;
  public Product() {}
  public Product(String name, double price, double priceDifference, String description, String imageURL)
  {
	  this.setName(name);
	  this.setPrice(price);
	  this.setPriceDifference(priceDifference);
	  this.setDescription(description);
	  this.setImageURL(imageURL);
  }
  @XmlElement(name="name")
  public String getName() {
	return name;
  }
  public void setName(String name) {
	this.name = name;
  }

  @XmlElement(name="price")
  public double getPrice() {
	return price;
  }
  public void setPrice(double price) {
	this.price = price;
  }
  @XmlElement(name="priceDifference")
  public double getPriceDifference() {
	return priceDifference;
  }
  public void setPriceDifference(double priceDifference) {
	this.priceDifference = priceDifference;
  }  
  @XmlElement(name="description")
  public String getDescription() {
	return description;
  }
  public void setDescription(String description) {
	this.description = description;
  }
  @XmlElement(name="imageURL")
  public String getImageURL() {
	return imageURL;
  }
  public void setImageURL(String imageURL) {
	this.imageURL = imageURL;
  }
}

