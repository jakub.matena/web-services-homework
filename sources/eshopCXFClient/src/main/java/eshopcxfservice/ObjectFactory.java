
package eshopcxfservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eshopcxfservice package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PublishNewDescription_QNAME = new QName("http://eshopCXFService/", "publishNewDescription");
    private final static QName _PublishNewDescriptionResponse_QNAME = new QName("http://eshopCXFService/", "publishNewDescriptionResponse");
    private final static QName _PublishNewImageURL_QNAME = new QName("http://eshopCXFService/", "publishNewImageURL");
    private final static QName _PublishNewImageURLResponse_QNAME = new QName("http://eshopCXFService/", "publishNewImageURLResponse");
    private final static QName _PublishNewPrice_QNAME = new QName("http://eshopCXFService/", "publishNewPrice");
    private final static QName _PublishNewPriceResponse_QNAME = new QName("http://eshopCXFService/", "publishNewPriceResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eshopcxfservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PublishNewDescription }
     * 
     */
    public PublishNewDescription createPublishNewDescription() {
        return new PublishNewDescription();
    }

    /**
     * Create an instance of {@link PublishNewDescriptionResponse }
     * 
     */
    public PublishNewDescriptionResponse createPublishNewDescriptionResponse() {
        return new PublishNewDescriptionResponse();
    }

    /**
     * Create an instance of {@link PublishNewImageURL }
     * 
     */
    public PublishNewImageURL createPublishNewImageURL() {
        return new PublishNewImageURL();
    }

    /**
     * Create an instance of {@link PublishNewImageURLResponse }
     * 
     */
    public PublishNewImageURLResponse createPublishNewImageURLResponse() {
        return new PublishNewImageURLResponse();
    }

    /**
     * Create an instance of {@link PublishNewPrice }
     * 
     */
    public PublishNewPrice createPublishNewPrice() {
        return new PublishNewPrice();
    }

    /**
     * Create an instance of {@link PublishNewPriceResponse }
     * 
     */
    public PublishNewPriceResponse createPublishNewPriceResponse() {
        return new PublishNewPriceResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PublishNewDescription }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PublishNewDescription }{@code >}
     */
    @XmlElementDecl(namespace = "http://eshopCXFService/", name = "publishNewDescription")
    public JAXBElement<PublishNewDescription> createPublishNewDescription(PublishNewDescription value) {
        return new JAXBElement<PublishNewDescription>(_PublishNewDescription_QNAME, PublishNewDescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PublishNewDescriptionResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PublishNewDescriptionResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://eshopCXFService/", name = "publishNewDescriptionResponse")
    public JAXBElement<PublishNewDescriptionResponse> createPublishNewDescriptionResponse(PublishNewDescriptionResponse value) {
        return new JAXBElement<PublishNewDescriptionResponse>(_PublishNewDescriptionResponse_QNAME, PublishNewDescriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PublishNewImageURL }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PublishNewImageURL }{@code >}
     */
    @XmlElementDecl(namespace = "http://eshopCXFService/", name = "publishNewImageURL")
    public JAXBElement<PublishNewImageURL> createPublishNewImageURL(PublishNewImageURL value) {
        return new JAXBElement<PublishNewImageURL>(_PublishNewImageURL_QNAME, PublishNewImageURL.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PublishNewImageURLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PublishNewImageURLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://eshopCXFService/", name = "publishNewImageURLResponse")
    public JAXBElement<PublishNewImageURLResponse> createPublishNewImageURLResponse(PublishNewImageURLResponse value) {
        return new JAXBElement<PublishNewImageURLResponse>(_PublishNewImageURLResponse_QNAME, PublishNewImageURLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PublishNewPrice }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PublishNewPrice }{@code >}
     */
    @XmlElementDecl(namespace = "http://eshopCXFService/", name = "publishNewPrice")
    public JAXBElement<PublishNewPrice> createPublishNewPrice(PublishNewPrice value) {
        return new JAXBElement<PublishNewPrice>(_PublishNewPrice_QNAME, PublishNewPrice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PublishNewPriceResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PublishNewPriceResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://eshopCXFService/", name = "publishNewPriceResponse")
    public JAXBElement<PublishNewPriceResponse> createPublishNewPriceResponse(PublishNewPriceResponse value) {
        return new JAXBElement<PublishNewPriceResponse>(_PublishNewPriceResponse_QNAME, PublishNewPriceResponse.class, null, value);
    }

}
