package eshop;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface PropertySetter {

    @javax.jws.WebMethod
    public boolean publishNewPrice(@WebParam(name="productName") String productName, @WebParam(name="price") double price);

    @javax.jws.WebMethod
    public boolean publishNewDescription(@WebParam(name="productName") String productName, @WebParam(name="description") String description);

    @javax.jws.WebMethod
    public boolean publishNewImageURL(@WebParam(name="productName") String productName, @WebParam(name="imageURL") String imageURL);
}
