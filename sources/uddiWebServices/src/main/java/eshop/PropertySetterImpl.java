package eshop;

import javax.jws.WebService;

import org.apache.juddi.v3.annotations.UDDIService;
import org.apache.juddi.v3.annotations.UDDIServiceBinding;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@UDDIService(
		businessKey="uddi:${keyDomain}:5720d5c4-3c13-4f10-86fe-05cf89f929ba",
		serviceKey="uddi:${keyDomain}:services_propertysetter", 
		description = "PropertySetter service")
@UDDIServiceBinding(
		bindingKey="uddi:${keyDomain}:${serverName}-${serverPort}-propertysetter-wsdl",
	    description="WSDL endpoint for the propertysetter Service.",
	    accessPointType="wsdlDeployment",
	    accessPoint="http://${serverName}:${serverPort}/uddiWebServices/services/propertysetter?wsdl")
@WebService(
		endpointInterface = "eshop.PropertySetter",
        serviceName = "PropertySetter")
public class PropertySetterImpl implements PropertySetter {

    private Set<String> products = new HashSet<>(Arrays.asList("Kindle 2020", "PocketBook 616"));

    @Override
    public boolean publishNewPrice(String productName, double price) {
        if (price < 1500)
            return false;
        return products.contains(productName);
    }

    @Override
    public boolean publishNewDescription(String productName, String description) {
        if (description.length() < 15)
            return false;
        return products.contains(productName);
    }

    @Override
    public boolean publishNewImageURL(String productName, String imageURL) {
        if (imageURL.length() < 10)
            return false;
        return products.contains(productName);
    }
}
