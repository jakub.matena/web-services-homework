package eshop2;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ProductProperties {

    @javax.jws.WebMethod
    public Product getProductPriceProperties(@WebParam(name="productName") String productName);

    @javax.jws.WebMethod
    public String getProductDescription(@WebParam(name="productName") String productName);

    @javax.jws.WebMethod
    public String getProductImageURL(@WebParam(name="productName") String productName);
}
