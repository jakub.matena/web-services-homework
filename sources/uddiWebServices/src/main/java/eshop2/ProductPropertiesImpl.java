package eshop2;

import javax.jws.WebService;

import org.apache.juddi.v3.annotations.UDDIService;
import org.apache.juddi.v3.annotations.UDDIServiceBinding;

import javax.jws.WebService;

import java.util.Map;
import java.util.HashMap;

@UDDIService(
		businessKey="uddi:${keyDomain}:5720d5c4-3c13-4f10-86fe-05cf89f929ba",
		serviceKey="uddi:${keyDomain}:services_productproperties", 
		description = "ProductProperties service")
@UDDIServiceBinding(
		bindingKey="uddi:${keyDomain}:${serverName}-${serverPort}-productproperties-wsdl",
	    description="WSDL endpoint for the productproperties Service.",
	    accessPointType="wsdlDeployment",
	    accessPoint="http://${serverName}:${serverPort}/uddiWebServices/services/productproperties?wsdl")
@WebService(
		endpointInterface = "eshop2.ProductProperties",
        serviceName = "ProductProperties")
public class ProductPropertiesImpl implements ProductProperties {

    private Map<String, Product> products = new HashMap<String, Product>() {{
    	        put("Kindle 2020", new Product("Kindle 2020", 2050, 15, "Basic Amazon Kindle device", "http://mydomain.cz/kindle-2020/image"));
    	        put("PocketBook 616",  new Product("PocketBook 616", 2130, 10, "Basic Pocketbook device", "http://mydomain.cz/pocketbook-616/image"));
    	    }};
    @Override
    public Product getProductPriceProperties(String productName) {
        Product result = products.get(productName);
        if (result == null)
            result = new Product();
        return result;
    }

    @Override
    public String getProductDescription(String productName) {
        Product product = products.get(productName);
        if (product == null)
            return "Not found";
        return product.description;
    }

    @Override
    public String getProductImageURL(String productName) {
        Product product = products.get(productName);
        if (product == null)
            return "Not found";
        return product.imageURL;
    }
}
