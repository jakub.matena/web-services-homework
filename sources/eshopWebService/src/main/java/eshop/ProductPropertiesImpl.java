package eshop;
import javax.jws.WebService;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebService(endpointInterface = "eshop.ProductProperties")
public class ProductPropertiesImpl implements ProductProperties {

    private Map<String, Product> products = Stream.of(new Object[][] {
            { "Kindle 2020", new Product("Kindle 2020", 2050, 15, "Basic Amazon Kindle device", "http://mydomain.cz/kindle-2020/image")},
            { "PocketBook 616",  new Product("PocketBook 616", 2130, 10, "Basic Pocketbook device", "http://mydomain.cz/pocketbook-616/image") },
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (Product) data[1]));

    @Override
    public Product getProductPriceProperties(String productName) {
        Product result = products.get(productName);
        if (result == null)
            result = new Product();
        return result;
    }

    @Override
    public String getProductDescription(String productName) {
        Product product = products.get(productName);
        if (product == null)
            return "Not found";
        return product.description;
    }

    @Override
    public String getProductImageURL(String productName) {
        Product product = products.get(productName);
        if (product == null)
            return "Not found";
        return product.imageURL;
    }
}
