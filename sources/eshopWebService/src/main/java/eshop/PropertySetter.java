package eshop;

import javax.jws.soap.SOAPBinding;

@javax.jws.WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface PropertySetter {

    @javax.jws.WebMethod
    public boolean publishNewPrice(String productName, double price);

    @javax.jws.WebMethod
    public boolean publishNewDescription(String productName, String description);

    @javax.jws.WebMethod
    public boolean publishNewImageURL(String productName, String imageURL);
}
