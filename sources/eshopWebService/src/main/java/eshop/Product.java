package eshop;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Product")
public class Product implements Serializable {
    String name;
    double price;
    double priceDifference;
    String description;
    String imageURL;

    Product(){}

    Product(String name, double price, double priceDifference, String description, String imageURL)  {
        this.name = name;
        this.price = price;
        this.priceDifference = priceDifference;
        this.description = description;
        this.imageURL = imageURL;
    }

    public String getName(){
        return name;
    }

    public double getPrice(){
        return price;
    }

    public double getPriceDifference(){
        return priceDifference;
    }

    public String getDescription() {return description;}

    public String getImageURL() {return imageURL;}

    @Override
    public String toString() {
        return name + price + priceDifference;
    }

}
