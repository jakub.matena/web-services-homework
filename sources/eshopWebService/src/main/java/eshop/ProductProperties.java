package eshop;

import javax.jws.soap.SOAPBinding;

@javax.jws.WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ProductProperties {

    @javax.jws.WebMethod
    public Product getProductPriceProperties(String productName);

    @javax.jws.WebMethod
    public String getProductDescription(String productName);

    @javax.jws.WebMethod
    public String getProductImageURL(String productName);
}
