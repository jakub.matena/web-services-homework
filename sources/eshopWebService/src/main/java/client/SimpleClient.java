package client;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.ws.Service;
import eshop.ProductProperties;

public class SimpleClient {
    public static void main(String[] args) throws Exception {
        /*URL url = new URL("http://127.0.0.1:8000/ProductProperties?wsdl");
        QName qname = new QName("http://eshop/", "ProductPropertiesImplService");
        Service service = Service.create(url, qname);
        ProductProperties properties = service.getPort(ProductProperties.class);
        System.out.println(properties.getProductDescription("Kindle 2020"));*/

        SOAPConnectionFactory soapcf = SOAPConnectionFactory.newInstance();
        SOAPConnection soapc = soapcf.createConnection();

        MessageFactory mf = MessageFactory.newInstance();
        SOAPMessage soapm = mf.createMessage();

        SOAPPart soapp = soapm.getSOAPPart();
        SOAPEnvelope soape = soapp.getEnvelope();
        SOAPBody soapb = soape.getBody();

        soape.getHeader().detachNode();
        QName name = new QName("http://tempuri.org/", "Add", "temp");
        SOAPElement soapel = soapb.addBodyElement(name);

        soapel.addChildElement(
                new QName("http://tempuri.org/", "intA", "temp")).addTextNode("3");
        soapel.addChildElement(
                new QName("http://tempuri.org/", "intB", "temp")).addTextNode("4");
        String endpoint = "http://localhost:8080/intermediary/TestServler";
        SOAPMessage response = soapc.call(soapm, endpoint);
        soapc.close();

        SOAPBody responseBody = response.getSOAPBody();
        if (responseBody.hasFault()) {
            System.out.println(responseBody.getFault().getFaultString());
        } else {

            QName AddResponseName = new QName("http://tempuri.org/", "AddResponse");
            QName AddResultName = new QName("http://tempuri.org/", "AddResult");

            SOAPBodyElement AddResponse = (SOAPBodyElement)
                    responseBody.getChildElements(AddResponseName).next();
            SOAPBodyElement AddResult = (SOAPBodyElement)
                    AddResponse.getChildElements(AddResultName).next();

            System.out.println(AddResult.getValue());


        }
    }
}