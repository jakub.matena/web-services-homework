package client;

import javax.xml.namespace.QName;
import javax.xml.soap.*;

public class PropetySetterClient {
    public static void main(String[] args) throws Exception {
        SOAPConnectionFactory soapcf = SOAPConnectionFactory.newInstance();
        SOAPConnection soapc = soapcf.createConnection();

        MessageFactory mf = MessageFactory.newInstance();
        SOAPMessage soapm = mf.createMessage();

        SOAPPart soapp = soapm.getSOAPPart();
        SOAPEnvelope soape = soapp.getEnvelope();
        SOAPBody soapb = soape.getBody();

        soape.getHeader().detachNode();
        QName name = new QName("http://eshop/", "publishNewPrice", "esh");
        SOAPElement soapel = soapb.addBodyElement(name);

        soapel.addChildElement(
                new QName("", "arg0", "")).addTextNode("Kindle 2020");
        soapel.addChildElement(
                new QName("", "arg1", "")).addTextNode("1600");
        String endpoint = "http://127.0.0.1:8000/PropertySetter";
        SOAPMessage response = soapc.call(soapm, endpoint);
        soapc.close();

        SOAPBody responseBody = response.getSOAPBody();
        if (responseBody.hasFault()) {
            System.out.println(responseBody.getFault().getFaultString());
        } else {

            QName publishNewPriceResponseName = new QName("http://eshop/", "publishNewPriceResponse");
            QName returnName = new QName("", "return");

            SOAPBodyElement publishNewPriceResponseReturn = (SOAPBodyElement)
                    responseBody.getChildElements(publishNewPriceResponseName).next();
            SOAPBodyElement returnElement = (SOAPBodyElement)
                    publishNewPriceResponseReturn.getChildElements(returnName).next();

            System.out.println(returnElement.getValue());


        }
    }
}
