package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import javax.xml.soap.*;
import javax.xml.namespace.QName;

/**
 * Servlet implementation class TestServler
 */
@WebServlet("/TestServler")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    private int getFactor(SOAPMessage requestMessage) throws SOAPException {
    	SOAPPart requestPart = requestMessage.getSOAPPart();
        SOAPEnvelope requestEnvelope = requestPart.getEnvelope();
        SOAPHeader requestHeader = requestEnvelope.getHeader();
        QName tweakQName = new QName("http://tweaks.com/", "tweak");
        Node tweak = (Node) requestHeader.getChildElements(tweakQName).next();
        String multiplicationFactor = tweak.getAttributes().getNamedItem("factor").getTextContent();
        int factor = Integer.parseInt(multiplicationFactor);
        return factor;
    }
    
    private void addTweakedHeader(SOAPMessage responseMessage, int factor) throws SOAPException {
		SOAPPart responsePart = responseMessage.getSOAPPart();
    	SOAPEnvelope responseEnvelope = responsePart.getEnvelope();
        
		SOAPHeader header = responseEnvelope.addHeader();
		QName tweakedS = new QName("http://tweaks.com/", "tweaked");
		SOAPElement tweaked = header.addChildElement(tweakedS);
		tweaked.addTextNode("Rounded to: " + factor);
    }
    
    private int roundByFactor(int number, int factor) {
    	return number - (number % factor);
    }
    
    private void tweak(SOAPMessage requestMessage, int factor) throws SOAPException {
    	SOAPBody responseBody = requestMessage.getSOAPBody();
        if (responseBody.hasFault()) {
            System.out.println(responseBody.getFault().getFaultString());
        } else {

			
            QName publishNewPriceName = new QName("http://eshop/", "publishNewPrice");
            QName arg1Name = new QName("", "arg1");

            SOAPBodyElement publishNewPrice = (SOAPBodyElement)
                    responseBody.getChildElements(publishNewPriceName).next();
            SOAPBodyElement arg1 = (SOAPBodyElement)
                    publishNewPrice.getChildElements(arg1Name).next();
            
            String svalue = arg1.getValue();
            int ivalue = Integer.parseInt(svalue);
            ivalue = roundByFactor(ivalue, factor);
            arg1.setValue(Integer.toString(ivalue));
        }
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse servletResponse) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		SOAPConnectionFactory soapcf;
		try {
			soapcf = SOAPConnectionFactory.newInstance();
			
			SOAPConnection soapc = soapcf.createConnection();
	
			MessageFactory mf = MessageFactory.newInstance();
			SOAPMessage requestMessage = mf.createMessage(null, request.getInputStream());
			
			int factor = getFactor(requestMessage);
			tweak(requestMessage, factor);
	        
			String endpoint = "http://127.0.0.1:8000/PropertySetter";
			SOAPMessage responseMessage = soapc.call(requestMessage, endpoint);
			soapc.close();
	        
			addTweakedHeader(responseMessage, factor);
			
			responseMessage.writeTo(servletResponse.getOutputStream());
	        

		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		//servletResponse.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
