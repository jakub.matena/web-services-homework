# Web Services Homework

## Eshop client
Connects to PropertySetter and sets price for a product using SAAJ.

## Eshop web service
Implementation of two services provided by Eshop (ProductProperties and PropertySetter). ProductProperties provides pricing, description and imageURL. 
PropertySetter modifies product's price, description and imageURL.

## Intermediary
SOAP Intermediary for Eshop web service. Processes tweak header that rounds the product price before it's set.  
Header tweak looks like this:  
    `<tweak factor="10" xmlns="http://tweaks.com/"/>`,  
where factor is used to round up the price.  
Resulting price is the maximal multiple of the factor not bigger than the given price.  
Client receives another header like this:  
    `<tweaked xmlns="http://tweaks.com/">Rounded to: 10</tweaked>`,  
informing about the change.

## Eshop CXF client

## Eshop CXF server 
Requires plaintext authentication

## UDDI Web Services
Eshop web services PropertySetter and ProductProperties supporting UDDI server registration

## BPEL Orchestrator
BPEL Orchestrator combining product name, price and image URL to create new description for the product.

## Eshop REST API
Eshop REST API enables to add, modify and delete products using REST API. Including Swagger documentation.

## UML representation of data graph
UML describing bindings between price comparer, eshops and price updater.
Turtle definion of RDFS eshop vocabulary and turtle representation if it's eshop and price updaters data.